class User < ActiveRecord::Base
  attr_accessor :activation_token

  validates :email, presence: true
  validates :first_name, presence: true
  validates :last_name, presence: true

  has_secure_password

  has_and_belongs_to_many :feeds

  before_create :create_activation_token

  def authenticated?(attribute, token)
    digest = send("#{attribute}_digest")
    return false if digest.nil?
    BCrypt::Password.new(digest).is_password?(token)
  end

 private

    def create_activation_token
      self.activation_token  = SecureRandom.urlsafe_base64
      self.activation_digest = create_digest(self.activation_token)
    end

    def create_digest(token)
      BCrypt::Password.create(token, cost: cost)
    end

    def cost
      if ActiveModel::SecurePassword.min_cost
        return BCrypt::Engine::MIN_COST
      else
        return BCrypt::Engine.cost
      end
    end



end
