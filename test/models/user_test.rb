require 'test_helper'

class UserTest < ActiveSupport::TestCase

  def setup
    @user = User.new(first_name: "Bart",
                     last_name: "Simpson",
                     email: "bart@simpsons.com",
                     password: "P@ssw0rd!",
                     password_confirmation: "P@ssw0rd!")
  end

  test "should be valid" do
    assert @user.valid?
  end

end
