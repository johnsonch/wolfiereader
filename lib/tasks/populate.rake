namespace :fake do
  desc "generating fake users"
  task :users => [:environment] do
    50.times do
      User.create(first_name: Faker::Name.first_name,
                  last_name: Faker::Name.last_name,
                  email: Faker::Internet.email,
                  activated: true,
                  activated_at: Time.now,
                  password: 'P@ssw0rd!',
                  password_confirmation: 'P@ssw0rd!')
    end
  end

  desc "generating fake feeds"
  task :feeds => [:environment] do
    feed_list = [
      "http://techrepublic.com.feedsportal.com/c/35463/f/670841/index.rss",
      "http://feeds.feedburner.com/techcrunch/startups?format=xml",
      "http://feeds.feedburner.com/TechCrunch/social?format=xml",
      "http://feeds.feedburner.com/TechCrunch/greentech?format=xml",
      "http://feeds.feedburner.com/TechCrunchIT?format=xml",
      "http://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml",
      "http://www.wired.com/feed/"
    ]
    User.all.each do |user|
      100.times do
         user.feeds.create(url: feed_list.shuffle[0],
                           name: Faker::Company.name)
      end
    end
  end


  desc "generating fake data"
  task :all_data => [:environment, :users, :feeds] do
  end
end
