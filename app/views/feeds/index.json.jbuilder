json.array!(@feeds) do |feed|
  json.extract! feed, :id, :name
  json.url user_feed_url(@user, feed, format: :json)
end
