Rails.application.routes.draw do

  resources :account_activations, only: [:edit]

  resources :users do
    resources :feeds
  end

  get 'about' => 'static_pages#about'
  get 'opensource' => 'static_pages#opensource'

  get 'login' => 'sessions#new'
  post 'login' => 'sessions#create'
  delete 'logout' => 'sessions#destroy'

  root 'static_pages#home'
end
