(function($) {
  var currentPage = 0;

  function loadData(data) {
    $('#feeds').append(Handlebars.compile("{{#feeds}} \
      <div class='well well-sm'> \
        <div class='row'> \
          <span class='col-sm-10 feed-name'><a href='{{url}}'>{{name}}</a></span> \
          <span class='col-sm-2'>  \
            <a data-confirm='Are you sure?' class='btn btn-danger' rel='nofollow' data-method='delete' href='{{url}}'>Remove</a> \
          </span> \
        </div> \
      </div> \
    {{/feeds}}")({ feeds: data }));
    if (data.length == 0) $('#next_page_spinner').hide();
  }

  function nextPageWithJSON() {
    currentPage += 1;
    var newURL = '/users/' + userId + '/feeds.json?page=' + currentPage;

    var splitHref = document.URL.split('?');
    var parameters = splitHref[1];
    if (parameters) {
      parameters = parameters.replace(/[?&]page=[^&]*/, "");
      newURL += '&' + parameters;
    }
    return newURL;
  }

  var loadingPage = 0;
  function getNextPage() {
    if (loadingPage != 0) return;

    loadingPage++;
    $.getJSON(nextPageWithJSON(), {}, updateContent).
      complete(function() { loadingPage-- });
  }

  function updateContent(response) {
    loadData(response);
  }

  function readyForNextPage() {
    if (!$('#next_page_spinner').is(':visible')) return;

    var threshold = 200;
    var bottomPosition = $(window).scrollTop() + $(window).height();
    var distanceFromBottom = $(document).height() - bottomPosition;

    return distanceFromBottom <= threshold;
  }

  function observeScroll(event) {
    if (readyForNextPage()) getNextPage();
  }

  $(document).scroll(observeScroll);

  getNextPage();
})(jQuery);
